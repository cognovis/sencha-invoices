# /packages/intranet-sencha-invoices-rest-procs

ad_library {
    Rest Procedures for the sencha-invoices package
    
    @author malte.sussdorff@cognovis.de
}


    
ad_proc -public im_rest_get_custom_sencha_invoices_autoselect {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on invoice, needed to autoselect feature
} {
    
    array set query_hash $query_hash_pairs
    
    if {![info exists query_hash(cost_type_id)]} {
        set query_hash(cost_type_id) 3250
    }
    # Get locate for translation
    set locale [lang::user::locale -user_id $rest_user_id]

    if {[info exists query_hash(source_cost_type_id)]} {
        set cost_type_id $query_hash(source_cost_type_id)
        set source_cost_type_where_clause "and c.cost_type_id = :cost_type_id"
    } else {
        set source_cost_type_where_clause ""
    }

    # -------------------------------------------------------
    # Valid variables to return for im_company
    set valid_vars [list invoice_id customer_name invoice_nr amount currency cost_type_id provider_name cost_type invoice_status due_date_calculated]

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}

    set valid_query_vars [list cost_status_id customer_id provider_id]


    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
    im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
    return
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    set cur_format [im_l10n_sql_currency_format]
    set date_format [im_l10n_sql_date_format]

    if {[info exists query_hash(company_id)]} {
        set company_id $query_hash(company_id)
    } else {
        set company_id [im_company_internal]
    }

    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } else {
        set project_id ""
    }

    if {[info exists query_hash(provider_id)]} {
        set provider_id $query_hash(provider_id)
        set company_id [im_company_internal]
    } else {
        set provider_id ""
    }

    if {[info exists query_hash(source_cost_type_id)]} {
        set source_cost_type_id $query_hash(source_cost_type_id)
    } else {
        set source_cost_type_id ""
    }

    if {[info exists query_hash(target_cost_type_id)]} {
        set target_cost_type_id $query_hash(target_cost_type_id)
    } else {
        set target_cost_type_id ""
    }


    if {"" == $company_id && "" != $project_id} {
        set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id" -default ""]
    } else  {
        set company_id $provider_id
    }

    set criteria [list]

    if {"" != $project_id} {
        set project_cost_ids_sql "
            select distinct cost_id
            from im_costs
            where project_id in (
                select  children.project_id
                from    im_projects parent,
                im_projects children
                where   children.tree_sortkey
                between parent.tree_sortkey
                and tree_right(parent.tree_sortkey)
                and parent.project_id = :project_id
            )
            UNION
            select distinct object_id_two as cost_id
            from acs_rels
            where object_id_one in (
                select  children.project_id
                from    im_projects parent,
                im_projects children
                where   children.tree_sortkey
                between parent.tree_sortkey
                and tree_right(parent.tree_sortkey)
                and parent.project_id = :project_id
            )
      "

      lappend criteria "
          i.invoice_id in (
          $project_cost_ids_sql
          )
        "
    }

    if {"" != $company_id && "" == $project_id} {
        if {$source_cost_type_id == [im_cost_type_invoice] || $source_cost_type_id == [im_cost_type_quote] || $source_cost_type_id == [im_cost_type_delivery_note] || $source_cost_type_id == [im_cost_type_interco_invoice] || $source_cost_type_id == [im_cost_type_interco_quote]} {
            lappend criteria "i.customer_id = :company_id"
        } else {
            lappend criteria "i.provider_id = :company_id"
        }
    }

    set project_where_clause [join $criteria " and\n            "]
    if { ![empty_string_p $project_where_clause] } {
        set project_where_clause " and $project_where_clause"
    }

    set order_by_clause "order by invoice_id DESC"

    set sql "
        select
        i.invoice_id,
        ci.cost_id,
        i.invoice_nr,
        i.payment_method_id,
        ci.currency,
        (to_date(to_char(i.invoice_date,:date_format),:date_format) + i.payment_days) as due_date_calculated,
        o.object_type,
        ci.amount as amount,
        ci.currency as currency,
        ci.paid_amount as payment_amount,
        ci.paid_currency as payment_currency,
        to_char(ci.amount,:cur_format) as invoice_amount_formatted,
        im_email_from_user_id(i.company_contact_id) as company_contact_email,
        im_name_from_user_id(i.company_contact_id) as company_contact_name,
        c.company_name as customer_name,
        c.company_path as company_short_name,
        p.company_name as provider_name,
        p.company_path as provider_short_name,
        im_category_from_id(i.invoice_status_id) as invoice_status,
        im_category_from_id(i.cost_type_id) as cost_type
    from
    im_invoices_active i,
    im_costs ci,
    acs_objects o,
    im_companies c,
    im_companies p,
        (
            select distinct
            cc.cost_center_id,
            ct.cost_type_id
            from    im_cost_centers cc,
            im_cost_types ct,
            acs_permissions p,
            party_approved_member_map m,
            acs_object_context_index c,
            acs_privilege_descendant_map h
            where
            p.object_id = c.ancestor_id
            and h.descendant = ct.read_privilege
            and c.object_id = cc.cost_center_id
            and p.privilege = h.privilege
            and p.grantee_id = m.party_id
            and ct.cost_type_id = :source_cost_type_id
        ) readable_ccs
    where
    i.invoice_id = o.object_id
    and i.invoice_id = ci.cost_id
    and i.customer_id = c.company_id
    and i.provider_id = p.company_id
    and ci.cost_type_id = :source_cost_type_id
    and ci.cost_center_id = readable_ccs.cost_center_id
    $project_where_clause
    $order_by_clause
    "

    set how_many 5000
    set start_idx 0
    set end_idx [expr $start_idx + $how_many - 1]

    set total_in_limited [db_string invoices_total_in_limited "
        select count(*)
            from ($sql) s
    "]

    set limited_query [im_select_row_range $sql $start_idx $end_idx]
    set selection "select z.* from ($limited_query) z $order_by_clause"
   # ad_return_complaint 1 $total_in_limited
    set true_invoices [list]
    set rel_type "im_invoice_invoice_rel"

    set value ""
    set result ""
    set obj_ctr 0

    set rest_otype_read_all_p [im_permission $rest_user_id "view_invoices"]

    db_foreach objects $selection {

    set linked_invoice_ids [db_list linked_invoices "select cost_id
        from im_costs c,
            (select object_id_one as linked_id from acs_rels where rel_type = :rel_type and object_id_two = :cost_id
                UNION
             select object_id_two linked_id from acs_rels where rel_type = :rel_type and object_id_one = :cost_id) l
        where c.cost_id = l.linked_id
        and c.cost_type_id = :target_cost_type_id"]

        if {[llength $linked_invoice_ids]>0} {continue}

        # Check permissions
        set read_p $rest_otype_read_all_p
        if {!$read_p} { continue }
        
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set dereferenced_result ""
        foreach v $valid_vars {
            eval "set a $$v"
            regsub -all {\n} $a {\n} a
            regsub -all {\r} $a {} a
            append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
        }

        append result "$komma{\"id\": \"$invoice_id\", \"object_name\": \"[im_quotejson $invoice_nr]\"$dereferenced_result}"
        incr obj_ctr
    }

    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_sencha_project: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_invoices_merge {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to merge provided bills
} {
    
    set user_id $rest_user_id

    array set query_hash $query_hash_pairs

    set required_vars [list source_invoice_id target_cost_type_id source_cost_type_id uploaded_file_path uploaded_file_name customer_id provider_id target_invoice_nr target_invoice_date payment_method_id payment_term_id invoice_currency]
    
    #still needs changes for required_vars & optional_vars
    foreach var $required_vars {
        if {![exists_and_not_null query_hash($var)]} {
            set $var ""
        } else {
            set $var $query_hash($var)
        }
    }

    #change comma seperated source_invoice_id to list
    set source_invoice_ids [split $source_invoice_id ","]
    
    #check if wasn't already used, using temporary list for that
    set rel_type "im_invoice_invoice_rel"
    set problematic_invoice_ids [list]
    foreach cost_id $source_invoice_ids {
        set linked_invoice_ids [db_list linked_invoices "select cost_id
            from im_costs c,
                (select object_id_one as linked_id from acs_rels where rel_type = :rel_type and object_id_two = :cost_id
                    UNION
                 select object_id_two linked_id from acs_rels where rel_type = :rel_type and object_id_one = :cost_id) l
            where c.cost_id = l.linked_id
            and c.cost_type_id = :target_cost_type_id"]
            if {[llength $linked_invoice_ids] > 0} {
               lappend problematic_invoice_ids $cost_id
            }
    }

    if {[llength $problematic_invoice_ids] > 0} {
        set problematic_invoice_ids_text ""
        set problematic_invoice_obj_ctr 0
        foreach problematic_invoice_id $problematic_invoice_ids {
            if {$problematic_invoice_obj_ctr == 0} {
                append problematic_invoice_ids_text "$problematic_invoice_id"
            } else {
                append problematic_invoice_ids_text ", $problematic_invoice_id"
            }
            incr problematic_invoice_obj_ctr
        }
        sencha_errors::add_error -object_id $user_id  -problem "Invoices $problematic_invoice_ids_text are already linked"
        sencha_errors::display_errors_and_warnings -object_id $user_id -action_type "display_error_and_refresh_store"
    }


    # ---------------------------------------------------------------
    # Deal with the target invoice nr
    # ---------------------------------------------------------------
    
    if {$target_invoice_nr eq ""} {
    # Check if we have uploaded a file, use the filename
    if {$uploaded_file_name ne ""} {
        set target_invoice_nr [file rootname $uploaded_file_name]
    }
    }

    if {![exists_and_not_null query_hash(cost_status_id)] || $cost_status_id eq ""} {

    # If we merge purchase orders to bills, the bill should be outstanding if we have a invoice
    # already provided by the provider
    if {$target_invoice_nr ne ""} {
        set cost_status_id [im_cost_status_outstanding]
    } else {
        set cost_status_id [im_cost_status_created]
    }
    }

    if {$target_invoice_date eq ""} {
        set effective_date $todays_date
    } else {
        set effective_date $target_invoice_date
    }

    # ---------------------------------------------------------------
    # Security
    # ---------------------------------------------------------------
    set current_cost_type_id $target_cost_type_id
    if {![im_permission $user_id add_invoices]} {
        sencha_errors::add_error -object_id $user_id  -problem "You don't have sufficient privileges to see this page"
        sencha_errors::display_errors_and_warnings -object_id $user_id -action_type "display_error_and_redirect" -modal_title "Insufficient Privileges"
    }

    foreach source_id $source_invoice_ids {
        set perm_proc [ad_parameter -package_id [im_package_invoices_id] "InvoicePermissionProc"]
    
        $perm_proc $user_id $source_id view_p read_p write_p admin_p

        if {!$read_p} {
            sencha_errors::add_error -object_id $user_id  -problem "You don't have sufficient privileges to see the source document"
            sencha_errors::display_errors_and_warnings -object_id $user_id -action_type "display_error_and_refresh_store" -modal_title "Insufficient Privileges"
        }
        set allowed_cost_type [im_cost_type_write_permissions $user_id]
        if {[lsearch -exact $allowed_cost_type $target_cost_type_id] == -1} {
            sencha_errors::add_error -object_id $user_id  -problem "You can't create documents of type #$target_cost_type_id."
            sencha_errors::display_errors_and_warnings -object_id $user_id -action_type "display_error_and_redirect" -modal_title "Insufficient Privileges"
        }
    }

    set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
    if {$payment_days eq ""} {
        set payment_days [ad_parameter -package_id [im_package_cost_id] "DefaultProviderBillPaymentDays" "" 30]
    }

    set todays_date [db_string get_today "select now()::date"]

    set project_ids [db_list projects "select distinct project_id from im_costs where cost_id in ([template::util::tcl_to_sql_list $source_invoice_ids]) and project_id is not null"]
        if {[llength $project_ids]>0} {
            set delivery_date [db_string delivery_date "select to_char(max(end_date),'YYYY-MM-DD') from im_projects where project_id in ([template::util::tcl_to_sql_list $project_ids])"]
            set project_id [lindex $project_ids 0]
        } else {
            set delivery_date ""
            set project_id ""
        }

    if {$delivery_date eq ""} {
        set delivery_date $effective_date
    }


    # ---------------------------------------------------------------
    # Determine whether it's an Invoice or a Bill
    # ---------------------------------------------------------------

    # Invoices and Quotes have a "Company" fields.
    set invoice_or_quote_p [im_cost_type_is_invoice_or_quote_p $target_cost_type_id]

    # Invoices and Bills have a "Payment Terms" field.
    set invoice_or_bill_p [im_cost_type_is_invoice_or_bill_p $target_cost_type_id]

    if {$invoice_or_quote_p} {
        set company_id $customer_id
        set company_type [_ intranet-core.Customer]
    } else {
        set company_id $provider_id
        set company_type [_ intranet-core.Provider]
    }

    # Get the company contact id
    set company_contact_id [im_invoices_default_company_contact $company_id]

    # Get the invoice office id
    set invoice_office_id [db_string company_main_office_info "select main_office_id from im_companies where company_id = :company_id" -default ""]

    # Get the correct cost_center_id
    set cost_center_id [db_string cost_center "select cost_center_id from im_costs where cost_id in ([template::util::tcl_to_sql_list $source_invoice_ids]) order by cost_center_id limit 1" -default ""]

    # Default for template: Get it from the company
    set template_id [im_invoices_default_company_template $target_cost_type_id $company_id $cost_center_id]
    if {$template_id eq "unknown"} {set template_id ""}

    # Get a reasonable default value for the vat_type_id,
    # either from the invoice or from the company.
    
    set vat_type_id [db_string vat_type_info "select max(vat_type_id) from im_costs where cost_id in ([template::util::tcl_to_sql_list $source_invoice_ids])" -default ""]
    if {"" == $vat_type_id} {
        set vat_type_id [db_string vat_info "select vat_type_id from im_companies where company_id = :company_id" -default ""]
    }


    # ---------------------------------------------------------------
    # Modify some variable between the source and the target invoice
    # ---------------------------------------------------------------

    # Old one: add an "a" behind the invoice_nt to indicate
    # a variant.
    # set invoice_nr [im_invoice_nr_variant $org_invoice_nr]

    # New One: Just create a new invoice nr
    # for the target FinDoc type.

    set invoice_nr $target_invoice_nr
    if {"" == $invoice_nr} {
        set invoice_nr [im_next_invoice_nr -cost_type_id $target_cost_type_id]
    }

    set new_invoice_id [im_new_object_id]

    # ---------------------------------------------------------------
    # Create invoice base data
    # ---------------------------------------------------------------

    # Let's create the new invoice
    if {[string length $invoice_nr] >40} {
    set invoice_nr [string range $invoice_nr 0 39]
    }

    if {[db_string exists_p "select 1 from im_invoices where invoice_nr = :invoice_nr" -default 0] || [ catch {
    set invoice_id [db_exec_plsql create_invoice "  select im_invoice__new (
          :new_invoice_id,      -- invoice_id
          'im_invoice',     -- object_type
          now(),            -- creation_date 
         :user_id,      -- creation_user
         '[ad_conn peeraddr]',  -- creation_ip
          null,         -- context_id
         :invoice_nr,       -- invoice_nr
         :company_id,       -- company_id
         :provider_id,      -- provider_id
         null,          -- company_contact_id
         :effective_date,       -- invoice_date
         'EUR',         -- currency
         :template_id,      -- invoice_template_id
                  :cost_status_id,  -- invoice_status_id
                  :target_cost_type_id,     -- invoice_type_id
                  :payment_method_id,   -- payment_method_id
                  :payment_days,        -- payment_days
                  0,            -- amount
                  null,         -- vat
                  null,         -- tax
                  ''            -- note
              )"]

    } err ] } {

    sencha_errors::add_error -object_id $new_invoice_id  -field "target_invoice_nr" -problem "Proviver Bill nr was already used"
    sencha_errors::display_errors_and_warnings -object_id $new_invoice_id -action_type "display_field_errors"
    return -level 2
    }
    # Give company_contact_id READ permissions - required for Customer Portal 
    permission::grant -object_id $invoice_id -party_id $company_contact_id -privilege "read"
    # Check if the cost item was changed via outside SQL
    im_audit -object_type "im_invoice" -object_id $invoice_id -action before_update -status_id $cost_status_id -type_id $target_cost_type_id

    # Get the vat from the vat_type_id
    if {"" != $vat_type_id} {
        set vat [db_string get_int1 "select aux_int1 from im_categories where category_id = :vat_type_id"]
    }

    # Update the invoice itself
    db_dml update_invoice "
    update im_invoices 
    set 
        invoice_nr  = :invoice_nr,
        payment_method_id = :payment_method_id,
        company_contact_id = :company_contact_id,
        invoice_office_id = :invoice_office_id
    where
        invoice_id = :invoice_id
    "


    db_dml update_costs "
    update im_costs
    set
         project_id = :project_id,
        cost_name   = :invoice_nr,
        customer_id = :company_id,
        cost_nr     = :invoice_id,
        provider_id = :provider_id,
        cost_status_id  = :cost_status_id,
        cost_type_id    = :target_cost_type_id,
        cost_center_id  = :cost_center_id,
        template_id = :template_id,
        effective_date  = :effective_date,
        delivery_date   = :delivery_date,
        start_block = ( select max(start_block) 
                from im_start_months 
                where start_block < :effective_date),
        payment_days    = :payment_days,
        payment_term_id = :payment_term_id,
        variable_cost_p = 't',
        amount      = null,
        currency    = :invoice_currency,
        vat_type_id     = :vat_type_id
    where
        cost_id = :invoice_id
    "

    # ---------------------------------------------------------------
    # Associate the invoice with the project via acs_rels
    # ---------------------------------------------------------------

    foreach project_id $project_ids {
        set rel_id [db_exec_plsql create_rel "      select acs_rel__new (
                 null,             -- rel_id
                 'relationship',   -- rel_type
                 :project_id,      -- object_id_one
                 :invoice_id,      -- object_id_two
                 null,             -- context_id
                 null,             -- creation_user
                 null             -- creation_ip
          )"]
    }

    # ---------------------------------------------------------------
    # Associate the invoice with the source invoices via acs_rels
    # ---------------------------------------------------------------

    foreach source_id $source_invoice_ids {
        if {$source_id >0} {
            set rel_id [db_exec_plsql create_invoice_rel "      select acs_rel__new (
                 null,             -- rel_id
                 'im_invoice_invoice_rel',   -- rel_type
                 :source_id,      -- object_id_one
                 :invoice_id,      -- object_id_two
                 null,             -- context_id
                 null,             -- creation_user
                 null             -- creation_ip
              )"]
        }
    }

    # ---------------------------------------------------------------
    # Create one invoice item per source invoice id
    # ---------------------------------------------------------------

    set sort_order 1
    foreach source_id $source_invoice_ids {

        if {$source_id > 0 } {
            db_1row source_invoice_info {
                select cost_name,project_id, amount, currency
                from im_costs where cost_id = :source_id
            }
            set item_id [db_nextval "im_invoice_items_seq"]

            set insert_invoice_items_sql "
            INSERT INTO im_invoice_items (
                item_id, item_name,
                project_id, invoice_id,
                item_units, item_uom_id,
                price_per_unit, currency,
                sort_order,
                item_source_invoice_id
            ) VALUES (
                :item_id, :cost_name,
                :project_id, :invoice_id,
                1, 322,
                :amount, :currency,
                :sort_order,
                :source_id
            )" 

            db_dml insert_invoice_items $insert_invoice_items_sql
           incr sort_order
        
        # Set the purchase order to status paid to mark it done
        db_dml update_po_status "update im_costs set cost_status_id = [im_cost_status_paid] where cost_id = :source_id"
        }
    }

    # ---------------------------------------------------------------
    # Update the invoice amount and currency 
    # ---------------------------------------------------------------

    set currencies [db_list distinct_currencies "
        select distinct
        currency
        from        im_invoice_items
        where   invoice_id = :invoice_id
        and currency is not null 
    "]

    if {1 != [llength $currencies]} {
        util_user_message -html -message "<b>[_ intranet-invoices.Error_multiple_currencies]:</b><br>
        [_ intranet-invoices.Blurb_multiple_currencies] <pre>$currencies</pre>"
    }

    set discount_perc 0.0
    set surcharge_perc 0.0

    # ---------------------------------------------------------------
    # Update the invoice value
    # ---------------------------------------------------------------

    im_invoice_update_rounded_amount \
        -invoice_id $invoice_id \
        -discount_perc $discount_perc \
        -surcharge_perc $surcharge_perc

    # --------------------------------------------------------------- 
    # Audit
    # ---------------------------------------------------------------

    im_audit -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id $cost_status_id -type_id $target_cost_type_id

    if {[exists_and_not_null uploaded_file_path]} {
       
        set file_extension [file extension $uploaded_file_name]
        #removing dot
        set file_extension [string range $file_extension 1 end] 

        set client_filename $uploaded_file_name
        #client_filename may be renamed with that callback
        callback intranet-invoices::merge_filename_change -invoice_id $invoice_id -file_extension $file_extension

        set source_folder [im_filestorage_cost_path $invoice_id]
        set source_dir "$source_folder"
        set dest_path "${source_dir}/$client_filename"
        file rename -force $uploaded_file_path $dest_path

        # --------------- Log the interaction --------------------
    
        db_dml insert_action "
        insert into im_fs_actions (
            action_type_id,
            user_id,
            action_date,
            file_name
        ) values (
            [im_file_action_upload],
            :user_id,
            now(),
            :dest_path || '/' || :client_filename
        )"
    }
    
    #notitication
    db_1row new_invoice "select invoice_nr from im_invoices where invoice_id =:invoice_id"
    db_1row provider "select company_name from im_companies where company_id =:provider_id"
    set invoice_link "[apm_package_url_from_key "intranet-invoices"]view?invoice_id=$invoice_id"
    set company_link "[apm_package_url_from_key "intranet-core"]companies/view?company_id=$provider_id"
    set notification_message "Provider bill <a href='$invoice_link'>$invoice_nr</a> has been created for <a href='$company_link'>$company_name</a>"
    im_freelance_notify -object_id $invoice_id -recipient_ids $user_id -message $notification_message
    #im_freelance_notify -object_id $invoice_id -recipient_ids 2125422 -message $notification_message

    set result "{\"success\": true,\n\"invoice_id\": \"$invoice_id\"} "
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_providers {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Old Handler for GET rest call to get possible providers for bill merging
} {
 
    set user_id $rest_user_id
    set customer_id [im_company_internal]

    #set sql_providers "select distinct cp.company_id, cp.company_name from im_companies cp, im_costs ci where ci.provider_id = cp.company_id and ci.customer_id = :customer_id and ci.cost_type_id = 3706 and cost_status_id = 3804"
    set sql_providers "select distinct ci.provider_id as company_id, cp.company_name from im_costs ci, im_companies cp where ci.provider_id = cp.company_id and ci.customer_id =:customer_id and ci.cost_type_id = 3706 and ci.cost_status_id = 3804"
    set providers ""
    set obj_ctr 0
    set rel_type "im_invoice_invoice_rel"
    
    db_foreach provider $sql_providers {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        #db_1row count "select count(*) as count from im_costs where customer_id =:company_id"
        append providers "$komma{\"provider_id\": \"$company_id\", \"provider_name\": \"[im_quotejson $company_name]\"}"
        incr obj_ctr
    }

    set result "{\"success\": true, \"total\": $obj_ctr, \n\"data\": \[\n$providers\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return   

}

ad_proc -public im_rest_get_custom_sencha_dynfields {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynfiels for given @param page_url
    To be finished, @param page_url is currently hardcoded
} {

    set user_id $rest_user_id
    set customer_id [im_company_internal]
    array set query_hash $query_hash_pairs

    set attributes [sencha_dynfields::elements -object_type "im_company" -page_url "/intranet-invoices/new-merge-invoiceselect"]
    set obj_ctr 0
    set attributes_json ""
    
    foreach var $attributes {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        append attributes_json "$komma{\"attribute_name\": \"[lindex $var 0]\", \"attribute_label\": \"[lindex $var 1]\", \"datatype\": \"[lindex $var 2]\", \"x_position\": \"[lindex $var 3]\", \"y_position\": \"[lindex $var 4]\", \"widget_name\": \"[lindex $var 5]\", \"label_style\": \"[lindex $var 6]\"}"
        incr obj_ctr
    }

    set result "{\"success\": true, \"total\": \"$obj_ctr\", \n\"data\": \[\n$attributes_json\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return   
}


ad_proc -public im_rest_get_custom_sencha_providers_with_po {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get providers with additional information
} {

    set user_id $rest_user_id
    set customer_id [im_company_internal]

    array set query_hash $query_hash_pairs

    set autocomplete_query_value ""
    if {[info exists query_hash(autocomplete_query)]} {
        set autocomplete_query_value $query_hash(autocomplete_query)
    }

    set attributes [sencha_dynfields::elements -object_type "im_company" -page_url "/intranet-invoices/new-merge-invoiceselect"] 
    set rel_type "im_invoice_invoice_rel"

    #build "in()" clause for companies from im_costs
    set providers_from_costs_sql "select ci.provider_id, ci.cost_id from im_costs ci where ci.customer_id =:customer_id and ci.cost_type_id=3706 and ci.cost_status_id in (3802,3804) and (select count(*) from acs_rels r where r.rel_type = :rel_type and r.object_id_one = ci.cost_id) = 0 and (select count(*) from acs_rels r2 where r2.rel_type = :rel_type and r2.object_id_two = ci.cost_id) = 0"
    set providers_list_sql ""
    append providers_list_sql " in("
    set iterator 0
    set already_in_there [list]

    #check for provider id as paremeter
    set url_provider_id ""
    if {[info exists query_hash(url_provider_id)]} {
        set url_provider_id $query_hash(url_provider_id)
    }    
    if {$url_provider_id ne ""} {
        append providers_list_sql "$url_provider_id"
        lappend already_in_there $url_provider_id
        incr iterator
    }
    
    db_foreach provider_from_costs $providers_from_costs_sql {
        if {[lsearch -exact $already_in_there $provider_id] < 1} {
            if {$iterator == 0} {
                append providers_list_sql "$provider_id"
            } else {
                append providers_list_sql " ,$provider_id"
            }
            lappend already_in_there $provider_id
            incr iterator
        }
    }
    append providers_list_sql ")"

    set obj_ctr 0
    set sql_providers_select "select cp.company_id as company_id, "
    foreach var $attributes {
        set attr_name [lindex $var 0]
        if {0 == $obj_ctr} {
            append sql_providers_select "cp.$attr_name as $attr_name"
        } else {
            append sql_providers_select ", cp.$attr_name as $attr_name"
        }
        incr obj_ctr
    }
    
    set autocomplete_sql ""
    if {$autocomplete_query_value ne ""} {
        set autocomplete_sql " and lower(company_name) like lower('$autocomplete_query_value%')"
    }

    set sql_providers_from "from im_companies cp"
    set sql_providers_where "where cp.company_id $providers_list_sql"
    set sql_providers_final "$sql_providers_select $sql_providers_from $sql_providers_where $autocomplete_sql"

    set providers ""
    set obj_ctr 0

    db_foreach provider $sql_providers_final {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append providers "$komma{"
        append providers "\"provider_id\": \"$company_id\", \"provider_name\": \"$company_name\",\n\"additional_attributes\": \[\n"
        set inner_obj_ctrl 1
        set count [llength $attributes]
        foreach var $attributes {
            set attr_name [lindex $var 0]
            eval "set attr_value $$attr_name"
            set pretty_name [lindex $var 1]
            set datatype [lindex $var 2]
            set x_pos [lindex $var 3]
            set y_pos [lindex $var 4]
            if {$inner_obj_ctrl == $count} {
                append providers "{\"name\": \"[im_quotejson $attr_name]\", \"value\": \"[im_quotejson $attr_value]\",  \"pretty_name\": \"[im_quotejson $pretty_name]\", \"datatype\": \"[im_quotejson $datatype]\", \"y_pos\": \"[im_quotejson $y_pos]\" }"
            } else {
                append providers "{\"name\": \"[im_quotejson $attr_name]\", \"value\": \"[im_quotejson $attr_value]\", \"pretty_name\": \"[im_quotejson $pretty_name]\",  \"datatype\": \"[im_quotejson $datatype]\", \"y_pos\": \"[im_quotejson $y_pos]\" },"
            }
            incr inner_obj_ctrl
        }
        append providers "\n\]}"
        incr obj_ctr
    }

    set result "{\"success\":true, \"total\": $obj_ctr, \n\"data\": \[\n$providers\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return  

}



ad_proc -public im_rest_get_custom_sencha_currencies {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls on available currencies, needed to autoselect feature
} {

    set enabled_only_p 1
    
    set enabled_sql "1=1"
    if {$enabled_only_p} { set enabled_sql "supported_p='t'" }

    set sql "select iso
         from currency_codes
         where $enabled_sql"

    set result ""
    set obj_ctr 0
    set dereferenced_result ""

    set default_currency [im_cost_default_currency]

    db_foreach currency $sql {
        switch $format {
                html {
                append result "<tr>
                    <td>$iso</td>
                </tr>\n"
                }
            json {
                set komma ",\n"
                if {0 == $obj_ctr} { set komma "" }
                set is_default false
                if {$default_currency eq $iso} {
                    set is_default true
                }
                append dereferenced_result "$komma{\"name\": \"[im_quotejson $iso]\",\"value\": \"[im_quotejson $iso]\", \"default\":$is_default}"
            }

        }
        incr obj_ctr
    }

    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"data\": \[\n$dereferenced_result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return


}

