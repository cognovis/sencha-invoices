--  upgrade-4.1.0.0.0-4.1.0.0.1.sql

SELECT acs_log__debug('/packages/sencha-invoices/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

UPDATE im_costs c SET cost_status_id = 3810 WHERE 
-- Limit to purchase_orders
cost_type_id = 3706
-- Limit to those linked to provider_bills
AND ((SELECT count(*)  FROM acs_rels r, im_costs c2 
	WHERE rel_type = 'im_invoice_invoice_rel' AND object_id_one = c.cost_id
	AND object_id_two = c2.cost_id AND c2.cost_type_id = 3704) > 0 
OR
(SELECT count(*)  FROM acs_rels r, im_costs c2
	WHERE rel_type = 'im_invoice_invoice_rel' AND object_id_two = c.cost_id
	and object_id_one = c2.cost_id AND c2.cost_type_id = 3704) > 0) ;
