ad_page_contract {
    upload_file
} {
}

set tmp_filename [ns_queryget upload_file.tmpfile]
set filename [ns_queryget upload_file]
file copy $tmp_filename "${tmp_filename}.copy"
set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"File: Data loaded\",\n\"data\": \[\n{\"file\":\"$filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n}"
ns_log Notice "Result:: $result"
im_rest_doc_return 200 "application/json" $result
return
