ad_page_contract {
    Shows invoices selector helper
} {
    {return_url ""}
    { customer_id:integer "" }
    { provider_id:integer "" }
    { project_id:integer "" }
    { start_idx:integer 0 }
    { how_many "" }
    { view_name "invoice_select" }
    { cost_status_id:integer ""}
    { source_cost_type_id:integer 3704}
    { target_cost_type_id:integer 3706}
}


# ---------------------------------------------------------------
# Security
# ---------------------------------------------------------------

set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id add_invoices]} {
    ad_return_complaint "Insufficient Privileges" "
    <li>You don't have sufficient privileges to see this page."    
}
# Make sure we can create invoices of target_cost_type_id...
set allowed_cost_type [im_cost_type_write_permissions $user_id]
if {[lsearch -exact $allowed_cost_type $target_cost_type_id] == -1} {
    ad_return_complaint "Insufficient Privileges" "
        <li>You can't create documents of type #$target_cost_type_id."
    ad_script_abort
}

set submit_button_text [lang::message::lookup "" intranet-invoices.Select_documents "Select Documents"]

if {$customer_id eq ""} {
    set customer_id [im_company_internal]
}

# ---------------------------------------------------------------
# "Sencha" conf
# ---------------------------------------------------------------

template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "/sencha-invoices/create-bill-from-po.js"

template::head::add_javascript -script "
    var serverParams = {
           serverUrl: '[ad_url]',
           defaultInvoiceSelectSorter:'due_date_calculated',
           defaultTargetCostTypeId:3704,
           defaultSourceCostTypeId:3706,
           defaultCustomerId:$customer_id,
           defaultCostStatusId:3804
    };
    serverParams.restUrl = serverParams.serverUrl + '/intranet-rest/';
    serverParams.auth = '';
"